<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Far West Sports Academy</title>

    <?php wp_head(); ?>

</head>

<body>

    <div class="container">
        <div class="row my-1">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex">
                            <?php if (has_custom_logo()) :
                                $custom_logo_id = get_theme_mod('custom_logo');
                                $image = wp_get_attachment_image_src($custom_logo_id, 'large');
                            ?>
                                <a href="<?php echo esc_url(home_url()); ?>">
                                    <img width="100" height="90" class="img-fluid" src="<?php echo $image['0']; ?>" alt="">
                                </a>
                            <?php endif ?>

                            <div class="fw-bold align-self-center mx-3 mt-2">
                                <h1 class="logo-txt h1-responsive d-none d-lg-block d-md-block d-sm-block"><b>Farwest Sports Academy</b></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
            
            </div>

        </div>
    </div>

    <div class="container-fluid sticky-top">
    <div class="row">
      <div class="col-md-12 bg-nav">
        <nav class=" container-fluid navbar-expand-lg brand-font">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#primarymenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <i class="fas fa-bars text-white"></i>

          </button>
          <?php
          wp_nav_menu(array(
            'theme_location'  => 'primary',
            'depth'           => 2,
            'container'       => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id'    => 'primarymenu',
            'menu_class'      => 'navbar-nav  justify-content-between w-100 primary-menu',
            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
            'walker'          => new WP_Bootstrap_Navwalker(),
          ));
          ?>
        </nav>

      </div>
    </div>
  </div>