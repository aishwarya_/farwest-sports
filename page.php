<?php get_header(); ?>
<div class="container py-4">
    <div class="category__header">
        <h1 class="category__title">
            <?php echo $wp_query->post->post_title; ?>
        </h1>
    </div>
    <div class="text-dark" style="text-align: justify; font-size:20px;">
        <?php
        if (have_posts()) : the_post();
            echo the_content();
        endif;
        ?>
    </div>
</div>
<?php get_footer(); ?>