<?php get_header(); ?>
<div class="container">
    <div class="category__header mt-4">
        <h1 class="category__title">
            <?php echo $wp_query->post->post_title; ?>
        </h1>
    </div>
    <div class="row my-5">
        <div class="col-md-8">
            <div class="card p-5 mb-5">
                <h3 class="text-center mb-3">Location</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3499.5513471308145!2d80.59687891508419!3d28.703064582390095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a1edb9b8277655%3A0x25ebd72b903d1061!2sHamro%20Dairy%20and%20Hamro%20Bakery!5e0!3m2!1sen!2snp!4v1634711720003!5m2!1sen!2snp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card p-5">
                <h3 class="text-center border-bottom">Leave Your Message</h3>
                <form>
                    <?php the_content(); ?>
                </form>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>