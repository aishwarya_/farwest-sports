<div class="container-fluid" style="background-color: #2151ab">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4 class="footer mt-3">Farwest Sports Academy</h4>
                <hr class="text-white">
                <div class="list brand-font">
                    <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'footer-menu',
                        'container'       => 'div',
                        'container_class' => 'footer-menu',
                        'menu_class' => '',
                    ));
                    ?>
                </div>

            </div>
            <div class="col-md-4">
                <h4 class="footer mt-3">Connect</h4>
                <hr class="text-white">
                <div class=" justify-content-between footer-icon">
                    <div class="facebook">
                        <a href="<?php echo get_theme_mod('facebook_link'); ?>"><i class="fab fa-facebook"></i> Facebook</a>
                    </div>
                    <div class="whatsApp">
                        <a href="<?php echo get_theme_mod('whatsApp_link'); ?>"><i class="fab fa-whatsapp"></i> WhatsApp</a>
                    </div>
                    <div class="instragram">
                        <a href="<?php echo get_theme_mod('instragram'); ?>"><i class="fab fa-instagram"></i> Instragram</a>
                    </div>
                    <div class="twitter">
                        <a href="<?php echo get_theme_mod('twitter'); ?>"> <i class="fab fa-twitter"></i> Twitter</a>
                    </div>
                    <div class="youtube">
                        <a href="<?php echo get_theme_mod('youtube'); ?>"><i class="fab fa-youtube"></i> Youtube</a>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <h4 class="footer mt-3">Contact Information</h4>
                <hr class="text-white">
                <div>
                    <div class=" justify-content-between footer-icon">
                        <div class="location">
                            <i class="fas fa-map-marker-alt mx-3"></i><?php echo get_theme_mod('location'); ?>
                        </div>
                        <div class="phone">
                            <i class="fas fa-phone-volume mx-3"></i><?php echo get_theme_mod('phone'); ?>
                        </div>
                        <div class="fax">
                            <i class="fas fa-fax mx-3"></i><?php echo get_theme_mod('fax'); ?>
                        </div>
                        <div class="email">
                            <i class="far fa-envelope mx-3"></i><?php echo get_theme_mod('email'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="background-color:#2151ab ">
            <div class="row">
                <hr class="text-white">
                <div class="col-md-6">
                    <h6 class="h5 text-white text-start">Copyright © 2021 Farwest Sports Academy.</h6>
                </div>
                <div class="col-md-6">
                    <h6 class="h5 text-white text-end">Developed by <a class="text-warning" href="https://mohrain.com/"> Mohrain WebSoft (P) Ltd.</a></h6>
                </div>
            </div>
        </div>
    </div>


    <?php wp_footer(); ?>

    </body>

    </html>