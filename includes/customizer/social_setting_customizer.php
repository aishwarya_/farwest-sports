<?php
function social_customize_register($wp_customize)
{
    $wp_customize->add_section('social_sites_section', array(
        'title' => __('Social Networks', ''),
        'priority' => 101
    ));
    // Settings
    $wp_customize->add_setting('facebook_link', array(
        'default' => '',
    ));

    $wp_customize->add_setting('whatsApp_link', array(
        'default' => '',
    ));

    $wp_customize->add_setting('twitter_link', array(
        'default' => '',
    ));
    
    $wp_customize->add_setting('instagram_link', array(
        'default' => '',
    ));
    $wp_customize->add_setting('youtube_link', array(
        'default' => '',
    ));
    
    $wp_customize->add_setting('phone', array(
        'default' => '',
    ));
    $wp_customize->add_setting('location', array(
        'default' => '',
    ));
    $wp_customize->add_setting('email', array(
        'default' => '',
    ));
    $wp_customize->add_setting('fax', array(
        'default' => '',
    ));
    // Controls
    $wp_customize->add_control('facebook_link_control', array(
        'label' => __('Facebook Link', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'facebook_link',
    ));
    $wp_customize->add_control('whatsApp_link_control', array(
        'label' => __('WhatsApp Link', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'whatsApp_link',
    ));
    $wp_customize->add_control('twitter_link_control', array(
        'label' => __('Twitter Link', 'social'),
        'type' => 'url',
        'section' => 'social_sites_section',
        'settings' => 'twitter_link',
    ));
    $wp_customize->add_control('instagram_link_control', array(
        'label' => __('Instragram Link', 'social'),
        'type'  => 'url',
        'section' => 'social_sites_section',
        'settings' => 'instagram_link',
    ));
    $wp_customize->add_control('youtube_link_control', array(
        'label' => __('YoutubeLink', 'social'),
        'type'  => 'url',
        'section' => 'social_sites_section',
        'settings' => 'youtube_link',
    ));
    $wp_customize->add_control('phone_control', array(
        'label' => __('phone', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'phone',
    ));
    $wp_customize->add_control('location_control', array(
        'label' => __('location', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'location',
    ));
    $wp_customize->add_control('email_control', array(
        'label' => __('email', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'email',
    ));
    $wp_customize->add_control('fax_control', array(
        'label' => __('fax', 'social'),
        'section' => 'social_sites_section',
        'settings' => 'fax',
    ));
}
add_action('customize_register', 'social_customize_register');