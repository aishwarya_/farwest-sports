<?php get_header(); ?>
<?php
$obj = get_queried_object();
?>
<div class="container py-4">
    <?php if ($obj->name) : ?>
        <div class="category__header">
            <h1 class="category__title"><?php echo $obj->name; ?></h1>
        </div>
    <?php endif; ?>

    <div class="row my-4">


        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'post',
            'cat' => $obj->term_id,
            'posts_per_page' => 9,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-4 mb-5">
                    <div class="card mb-3 cards" style="height: 400px;">
                        <div class="row">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img class="img-fluid rounded-start event-img" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                            </a>
                            <div class="card-body">
                                <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                    <h5 class="card-title text-dark "><?php the_title(); ?></h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>

    <!-- Pagination -->
    <?php get_template_part('partials/page', 'links'); ?>

</div>

<?php get_footer(); ?>