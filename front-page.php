<?php get_header(); ?>

<!-- Carouseel -->

<div class="container-fluid">
    <div class="row">

        <div id="carouselExampleIndicators" class="carousel slide p-0" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>
            </div>
            <div class="carousel-inner">

                <?php
                $args = array(
                    'post_type' => 'slider',
                    'posts_per_page' => 5,
                    // 'order' => 'ASC'
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    $i = 0;
                    while ($latest->have_posts()) : $latest->the_post();
                        $i++;
                ?>
                        <div class="<?php if ($i == 1) echo 'active' ?> carousel-item" data-bs-interval="2000">

                            <?php if (has_post_thumbnail()) : ?>
                                <div class="text-center mb-4">
                                    <img width="100%" style="aspect-ratio: 4/2;" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                                </div>
                            <?php endif; ?>

                            <div class="carousel-caption">
                                <h1><?php the_title(); ?></h1>
                            </div>
                        </div>

                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>

            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <!-- query -->
        <?php
        $args = array(
            'post_type' => 'msg',
            'posts_per_page' => 1,
            // 'order' => 'ASC'
        );
        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();
        ?>

                <div class="col-md-6 mt-4">
                    <a class="text-dark" href="/farwest_sports/index.php/about-us/">
                        <h1><?php the_title(); ?></h1>
                    </a>
                    <p><?php the_content(); ?></p>
                </div>
                <div class="col-md-6">
                    <?php if (has_post_thumbnail()) : ?>
                        <div class="">
                            <img class="img-aspect" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                        </div>
                    <?php endif; ?>
                </div>

        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>

    </div>
</div>

<div class="container">
    <div class="row my-5">
        <h1 class="text-center mb-2" style="color:green;">Our Programs</h1>
        <div class="line">
            <div class="row my-2 p-3">
                <div class="col-md-4 bg-primary p-1"></div>
                <div class="col-md-4 bg-primary p-1"></div>
                <div class="col-md-4 bg-primary p-1"></div>
            </div>
        </div>

        <!-- query -->
        <?php
        $args = array(
            'post_type' => 'sport',
            'posts_per_page' => 6,
            // 'order' => 'ASC'
        );
        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();
        ?>

                <div class="col-md-2">
                    <div class="">
                        <a href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <img class="img-aspect" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                            <?php endif; ?>
                        </a>
                        <div class="card-body">
                            <a href="<?php the_permalink(); ?>">
                                <h4 class="card-title text-dark brand-font"><?php the_title(); ?></h4>
                            </a>
                        </div>
                    </div>

                </div>

        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>

    </div>
</div>

<?php get_footer(); ?>