<?php

require_once get_template_directory() . '/includes/customizer/social_setting_customizer.php';

// registering navwalker
function register_navwalker()
{
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

// links
if (!function_exists('theme_enquee_scripts')) {
    function theme_enquee_scripts()
    {

        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

        wp_enqueue_style('mdb', get_template_directory_uri() . '/mdb/css/mdb.min.css');

        wp_enqueue_style('utils', get_template_directory_uri() . '/css/utils.css');

        wp_enqueue_style('fontawesome', get_template_directory_uri() . '/fontawesome/css/all.css');

        wp_enqueue_style('font_cdn', 'https://fonts.googleapis.com/css2?family=Noto+Sans:wght@400;700&display=swap');

        wp_enqueue_script('JQuery', get_template_directory_uri() . '/js/jquery-3.6.0.min.js');

        wp_enqueue_script('bootstrap_JS', get_template_directory_uri() . '/js/bootstrap.min.js');

        wp_enqueue_script('mdb_js', get_template_directory_uri() . '/mdb/js/mdb.min.js');
    }
}
add_action('wp_enqueue_scripts', 'theme_enquee_scripts');

// for logo
function themename_custom_logo_setup()
{
    $defaults = array(
        'height'      => 120,
        'width'       => 200,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'themename_custom_logo_setup');

// for menu

add_theme_support('menus');
function wp_theme_setup()
{
    register_nav_menus(array(
        'primary' => __('Primary Menu', 'primary menu'),
        // 'secondary' => __('Secondary Menu', 'secondary menu'),

        'footer-menu' => __('Footer Menu', 'footer menu'),

        'mobile-menu' => __('Mobile Menu Location', 'Mobile Menu'),
    ));
}
add_action('after_setup_theme', 'wp_theme_setup');

// for custom image

if( !defined(THEME_IMG_PATH)){
    define( 'THEME_IMG_PATH', get_stylesheet_directory_uri() . '/image' );
   }

   // for static image 
add_theme_support('post-thumbnails');